import paho.mqtt.client as mqtt
import time
import json
import configparser
import sferaconfig
from pymongo import MongoClient


sample_time = sferaconfig.getConfig("reporter_sample_time")


def on_message(client, userdata, msg):
    global nextSampleTime
    global sample_time
    now = int(time.time())
    if nextSampleTime < now :
        message = json.loads(msg.payload.decode('UTF-8'))
        if message.get("event", None) == "sfera_status":
            global samples
            d = message["data"]
            d["time"] = now
            client = MongoClient('mongodb://localhost:27017/')
            db = client['sfera']
            samples = db.samples
            samples.insert_one(d)
            client.close()
            nextSampleTime = now + sample_time


def on_connect(client, userdata, flags, rc):
    print("Connected with result code "+str(rc))
    client.subscribe("local/status")

def on_disconnect(client, userdata, rc):
    if rc != 0:
        now=time.strftime("%Y-%m-%d %H:%M")
        print("Unexpected disconnection."+now)

client = mqtt.Client(client_id="reporter")

client.on_connect = on_connect
client.on_message = on_message
client.on_disconnect = on_disconnect
client.connect("localhost", 1883, 60)

client.loop_start()
nextSampleTime = 0
while True:
    pass
